#include "common.h"

uint8_t uart_buff[UART_BUFF_SIZE] = {0};
uint8_t *uart_buff_ptr = uart_buff;

unsigned int InitUART(const int baudRate, const WifiIotUartExtraAttr *extraAttr)
{
    WifiIotUartAttribute uart_attr = {

        //baud_rate: 9600
        .baudRate = baudRate,

        //data_bits: 8bits
        .dataBits = 8,
        .stopBits = 1,
        .parity = 0,
    };

    //Initialize uart driver
    return UartInit(UART_PORT, &uart_attr, extraAttr);
}

void PrintfUART(char* str){
    //UartWrite(WIFI_IOT_UART_IDX_0, (unsigned char *)str, strlen(str));  
    /*while (*str!=0){
        //printf("%02X ",(int)*str);
        str++;
    }*/
    printf(str);
}

unsigned char* ScanfUART(void){    
    if (uart_buff_ptr!=NULL){
        free(uart_buff_ptr);
        memset(uart_buff, 0, UART_BUFF_SIZE);
        uart_buff_ptr = uart_buff;
    }
    UartRead(WIFI_IOT_UART_IDX_0, uart_buff_ptr, UART_BUFF_SIZE);
    return uart_buff_ptr;
}