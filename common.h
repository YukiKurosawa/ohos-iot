//Standard C Library
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "malloc.h"

//OHOS Library
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "wifiiot_errno.h"
#include "wifiiot_gpio.h"
#include "wifiiot_gpio_ex.h"
#include "wifiiot_adc.h"
#include "wifiiot_uart.h"


//Common Include
#define UART_TASK_STACK_SIZE 1024 * 8
#define UART_TASK_PRIO 25
#define UART_BUFF_SIZE 1000
#define UART_PORT WIFI_IOT_UART_IDX_0

//Common Function
unsigned int InitUART(const int baudRate, const WifiIotUartExtraAttr *extraAttr);
void PrintfUART(char* str);
unsigned char* ScanfUART(void);


