#include "bootstrap.h"

void bootstrap_main(void){

    #ifdef CONFIG_H_BOOTLOGO
        showBootLogo();
    #endif
    
    #ifdef CONFIG_H_SHELL
        startShell();
    #else
        printf("NO SHELL SUPPORT HERE\r\n");
        usleep(3600*1000*1000);
    #endif
}

void showBootLogo(void){
    printf(BOOT_LOGO_FIRMWARE);
}

void startShell(void){
    osThreadAttr_t attr;

    attr.name = "ShellTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 1024*50;
    attr.priority = osPriorityNormal;

    if (osThreadNew((osThreadFunc_t)ShellTask, NULL, &attr) == NULL) {
        printf("[Shell] Falied to create ShellTask!\n");
    }
}

void *ShellTask(void* arg){
    (void)arg;
    osDelay(100);
    shell_init();
    return NULL;
}