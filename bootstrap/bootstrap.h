#include "../common.h"
#include "../config.h"
#include "../hish/kernel.h"

#define BOOT_LOGO_FIRMWARE "\
****************\r\n\
*   OHOS 1.0   *\r\n\
*  HI3861V100  *\r\n\
****************\r\n\
\r\n\
SYSTEM WILL START\r\n\
IN 1 SECOND.     \r\n\
"

void bootstrap_main(void);
void showBootLogo(void);
void startShell(void);
void *ShellTask(void* arg);