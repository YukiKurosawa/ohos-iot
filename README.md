OHOS-IOT

# Introduction
-----------------
This is an application which is built for Hi3861 IOT Boards.
This application provides a shell-like experience on UART0.

# Current Version
-----------------
0.0.1-alpha (build 0012)

# Current Status
-----------------
Boot Logo: OK
UART Basic IO: OK
Shell Kernel: Under Construction
Shell Commands: Wait for develop

# Third Party Libraries
-----------------
OHOS SDK for Hi3861: 
(C) Copyright Huawei Corporation
License: Apache V2

Hi3861_WIFI_IOT_SAMPLE: 
(C) Copyright Hihope.Org
License: Apache V2

UART IO Demo: 
BearPi Community
License: Apache V2

Special Thanks:
Huawei Corporation
Hihope.org
BearPi Community